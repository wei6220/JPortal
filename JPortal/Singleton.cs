﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JPortal
{
    public class Singleton
    {
        //private static readonly Singleton _instance = new Singleton();
        //Lazy惰性加載，實際叫用才載入實體
        private static readonly Lazy<Singleton> _instance = new Lazy<Singleton>(() => new Singleton());
        private Singleton() { }

        public static Singleton Instance
        {
            get
            {
                return _instance.Value;
            }
        }
      
        public void DoSometing()
        {
            Console.WriteLine("Do Someting ....");
        }
    }
}