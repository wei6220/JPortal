﻿using JPortal.Components;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using CommonLibrary;

namespace JPortal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Singleton.Instance.DoSometing();
            var cookie = Request.Cookies;// Request.Headers.GetCookies()[0]["jj"];

            //var httphelper = new CommonLibrary.HttpHelper();
            //var qq = httphelper.Get("http://localhost:57002/uas/api/auth/gettoken?id=4610");

            //return Redirect("~/login/index.html");


            //var httpHelper = new HttpHelper();
            //var result = httpHelper.Post2("http://localhost:57002/uas/api/auth/GetToken?id=jerry.chen@dfi.com", null, HttpHelper.ContnetTypeEnum.Json);

            return View();
        }

        public ActionResult Download()
        {
            string fileName = "SHFBGuidedInstaller_2015.01.12.0.zip";
            var path = Path.Combine(Server.MapPath("~/uploads"), fileName);
            var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            //byte[] fileStream = System.IO.File.ReadAllBytes(path);
            return File(fileStream, "application/octet-stream", fileName);


            //var path = Path.Combine("~/software", fileName);
            //var path = Path.Combine(Server.MapPath("/software"), fileName);
            //var path = Path.Combine("/desktop", fileName);
            //var file = new FileInfo(path);

            //WebRequest request = WebRequest.Create("http://localhost:8085/software/" + fileName);
            //using (WebResponse res = request.GetResponse())
            //{
            //    using (Stream stream = res.GetResponseStream())
            //    {
            //        //TransmitFile(stream, fileName, res.ContentLength.ToString());
            //        Response.Clear();
            //        Response.ContentType = "application/octet-stream";
            //        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            //        Response.AddHeader("Content-Length", res.ContentLength.ToString());
            //        //using (FileStream writeStream = new FileStream(Server.MapPath("~/uploads/") + fileName, FileMode.Create))
            //        //{
            //        byte[] buffer = new byte[2048];
            //        int bytesRead = stream.Read(buffer, 0, 2048);
            //        while (bytesRead > 0)
            //        {
            //            //writeStream.Write(buffer, 0, bytesRead);
            //            int partailLength = stream.Read(buffer, 0, 2048);
            //            Response.OutputStream.Write(buffer, 0, partailLength);
            //            Response.Flush();
            //            bytesRead = stream.Read(buffer, 0, 2048);
            //        }
            //        stream.Close();
            //        //}
            //    }
            //}


            //string fileName = "MMTOOL.rar";
            // Get the object used to communicate with the server.  
            //FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://13.94.42.85/" + fileName);
            //request.Method = WebRequestMethods.Ftp.DownloadFile;
            //request.Credentials = new NetworkCredential("ftptest", "DFIabc123");
            //FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            //using (Stream responseStream = response.GetResponseStream())
            //{
            //    TransmitFile(responseStream, fileName);
            //}

            //string localPath = Server.MapPath("~/uploads");
            //FtpWebRequest requestFileDownload = (FtpWebRequest)WebRequest.Create("ftp://13.94.42.85/" + fileName);
            //requestFileDownload.Credentials = new NetworkCredential("ftptest", "DFIabc123");
            //requestFileDownload.Method = WebRequestMethods.Ftp.DownloadFile;
            //FtpWebResponse responseFileDownload = (FtpWebResponse)requestFileDownload.GetResponse();
            //Stream responseStream = responseFileDownload.GetResponseStream();
            //Response.Clear();
            //Response.ContentType = "application/octet-stream";
            //Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            ////Response.AddHeader("Content-Length", responseStream.Length.ToString());
            //using (FileStream writeStream = new FileStream(localPath + "/" + fileName, FileMode.Create))
            //{
            //    byte[] buffer = new byte[2048];
            //    int bytesRead = responseStream.Read(buffer, 0, 2048);
            //    while (bytesRead > 0)
            //    {
            //        writeStream.Write(buffer, 0, bytesRead);
            //        int partailLength = writeStream.Read(buffer, 0, 2048);
            //        Response.OutputStream.Write(buffer, 0, partailLength);
            //        Response.Flush();
            //        bytesRead = responseStream.Read(buffer, 0, 2048);
            //    }
            //    responseStream.Close();
            //}
            //requestFileDownload = null;
            //responseFileDownload = null;
            //TransmitFile(localPath + fileName, fileName);

            //return null;
        }

        private void TransmitFile(string fullPath, string outFileName)
        {
            Stream stream = null;
            // Buffer to read 10K bytes in chunk:
            byte[] readBuffer = new byte[10000];
            int partailLength;
            long dataToRead;
            string filePath = fullPath;
            string fileName = Path.GetFileName(filePath);
            try
            {
                stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                dataToRead = stream.Length;
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + outFileName);
                Response.AddHeader("Content-Length", stream.Length.ToString());
                while (dataToRead > 0)
                {
                    // Verify that the client is connected.
                    if (Response.IsClientConnected)
                    {
                        readBuffer = new byte[10000];
                        partailLength = stream.Read(readBuffer, 0, 10000);
                        Response.OutputStream.Write(readBuffer, 0, partailLength);
                        // Flush the data to the output.
                        Response.Flush();
                        dataToRead = dataToRead - partailLength;
                    }
                    else
                    {
                        dataToRead = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
                Response.Close();
            }
        }

        private void TransmitFile(Stream ostream, string outFileName,string contentlength)
        {
            Stream stream = null;
            // Buffer to read 10K bytes in chunk:
            byte[] readBuffer = new byte[10000];
            int partailLength;
            long dataToRead;
            //string filePath = fullPath;
            //string fileName = Path.GetFileName(filePath);
            try
            {
                stream = (FileStream)ostream;
                dataToRead = stream.Length;
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + outFileName);
                Response.AddHeader("Content-Length", contentlength);
                while (dataToRead > 0)
                {
                    // Verify that the client is connected.
                    if (Response.IsClientConnected)
                    {
                        readBuffer = new byte[10000];
                        partailLength = stream.Read(readBuffer, 0, 10000);
                        Response.OutputStream.Write(readBuffer, 0, partailLength);
                        // Flush the data to the output.
                        Response.Flush();
                        dataToRead = dataToRead - partailLength;
                    }
                    else
                    {
                        dataToRead = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
                Response.Close();
            }
        }


        [HttpPost]
        public ActionResult Upload(IEnumerable<HttpPostedFileBase> files)
        {

            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                var fileName = Path.GetFileName(file.FileName);
                //var path = Path.Combine(Server.MapPath("~/Junk/"), fileName);
                //file.SaveAs(path);
            }

            foreach (string file in Request.Files)
            {
                var FileDataContent = Request.Files[file];
                if (FileDataContent != null && FileDataContent.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(FileDataContent.FileName);
                    var path = Path.Combine(Server.MapPath("~/uploads"), fileName);
                    FileDataContent.SaveAs(path);
                }
            }

            foreach (var file in files)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/uploads"), fileName);
                    file.SaveAs(path);

                    //FileUnit fileUnit = new FileUnit();
                    //fileUnit.UploadFtp(file.InputStream, file.FileName);
                }
            }
            return Json(new { status="success" }); //RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UploadWithJson(qq files)
        {
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //dynamic c = js.Deserialize<dynamic>(files);

            //var name = files.name;
            try
            {
                byte[] newBytes = Convert.FromBase64String(files.bin.Split(',')[1]);
            }
            catch (Exception e)
            {

            }
            //var Formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            //using (var stream = new System.IO.MemoryStream(ObjectToByteArray(files)))
            //{
            //    var qq = Formatter.Deserialize(stream);
            //}

            //return RedirectToAction("Index");
            var jsonResult = Json(new { title = "qq" }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        [Serializable]
        public struct qq
        {
            public string name { get; set; }
            public string size { get; set; }
            public string bin { get; set; }
        }

        byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }


        [HttpPost]
        public ActionResult PartialUpload()
        {
            var fileName = "";
            foreach (string file in Request.Files)
            {
                var FileDataContent = Request.Files[file];
                if (FileDataContent != null && FileDataContent.ContentLength > 0)
                {
                    var stream = FileDataContent.InputStream;
                    fileName = Path.GetFileName(FileDataContent.FileName);
                    var UploadPath = Server.MapPath("~/uploads");
                    Directory.CreateDirectory(UploadPath);
                    string path = Path.Combine(UploadPath, fileName);
                    try
                    {
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);
                        using (var fileStream = System.IO.File.Create(path))
                        {
                            stream.CopyTo(fileStream);
                        }
                        FileUnit fileUnit = new FileUnit();
                        fileUnit.MergeFile(path);
                    }
                    catch (IOException ex)
                    {
                        // handle
                    }
                }
            }

            //return new HttpResponseMessage()
            //{
            //    StatusCode = HttpStatusCode.OK,
            //    Content = new StringContent("~/App_Data/uploads"),
            //};

            return Content("~/uploads" + fileName);
        }

        // TODO: Protect this route with the sign in policy
        [Authorize]
        public ActionResult Claims()
        {
            Claim displayName = ClaimsPrincipal.Current.FindFirst(ClaimsPrincipal.Current.Identities.First().NameClaimType);
            ViewBag.DisplayName = displayName != null ? displayName.Value : string.Empty;
            return View();
        }

        public ActionResult Error(string message)
        {
            ViewBag.Message = message;

            return View("Error");
        }
    }
}