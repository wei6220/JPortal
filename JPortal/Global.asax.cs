﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace JPortal
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void Application_BeginRequest(Object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;
            var request = context.Request;

        }

        private void Application_AuthenticateRequest(Object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;
            var request = context.Request;

        }

        private void Application_EndRequest(Object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;
            var request = context.Request;

            //var action = request.Url.LocalPath.Split('/')[request.Url.LocalPath.Split('/').Length - 1];
            //if (action != null && action.ToLower() == "signin")
            //{
            //    context.GetOwinContext().Authentication.Challenge(
            //       new AuthenticationProperties() { RedirectUri = "/" }, Startup.SignInPolicyId);
            //}
            //else if (action != null && action.ToLower() == "signout")
            //{
            //    IEnumerable<AuthenticationDescription> authTypes = context.GetOwinContext().Authentication.GetAuthenticationTypes();
            //    context.GetOwinContext().Authentication.SignOut(authTypes.Select(t => t.AuthenticationType).ToArray());
            //    request.GetOwinContext().Authentication.GetAuthenticationTypes();
            //}

        }

        public void Application_Error(Object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;
            var request = context.Request;

            var error = context.Error;

        }

    }
}
