﻿var DfiOwApi = (function () {
    var instance;

    function createInstance(options) {
        options = options || {
            hosturl: 'http://localhost:64275',
            btocurl: 'dfitestb2c.onmicrosoft.com',
            //clientid: '3d0cab7d-b082-46bc-90e7-b7bf2fb262f6',
            //redirecturl: 'https%3A%2F%2Fdfidnn.azurewebsites.net%2F',
            clientid: '385204c8-7d94-4393-ad66-21a50c92fc7c',
            redirecturl: 'https%3A%2F%2Flocalhost%3A44348%2F',
            nonce: '12345',
        };
        var object = new Dfiow(options);
        return object;
    }

    return {
        getInstance: function (options) {
            if (!instance) {
                instance = createInstance(options);
            }
            return instance;
        }
    };
})();

function Dfiow(options) {
    this.host_url = options.hosturl || "";
    this.client_id = options.clientid || "";
    this.redirect_uri = options.redirecturl || "";
    this.nonce = options.nonce || "";
    this.btoc_url = options.btocurl || "";
    this.b2c_token = this.getCookie() || "";
}

Dfiow.prototype.setCookie = function (idtoken) {
    var result;
    var options = {
        type: "GET",
        url: this.host_url + "/DesktopModules/PartnerZone/API/cookie/set?idtoken=" + idtoken,
        async: false,
        xhrFields: { withCredentials: true },
        success: function (data) {
            result = data;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            result = thrownError;
        }
    };
    $.ajax(options);
    return result;
};
Dfiow.prototype.getCookie = function () {
    var result = "";
    var options = {
        type: "GET",
        url: this.host_url + "/DesktopModules/PartnerZone/API/cookie/get",
        async: false,
        xhrFields: { withCredentials: true },
        success: function (data) {
            result = data;
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    };
    $.ajax(options);
    return result;
};

Dfiow.prototype.getDFIAzureB2CURL = function () {
    var url = "https://login.microsoftonline.com/" + this.btoc_url + "/oauth2/v2.0/authorize?";
    url += "client_id=" + this.client_id;
    url += "&response_type=code+id_token";
    url += "&redirect_uri=" + this.redirect_uri;
    url += "&response_mode=form_post";
    url += "&scope=openid%20offline_access";
    url += "&state=" + encodeURI(location.protocol + '//' + location.host);
    url += "&nonce=" + this.nonce;
    url += "&p=B2C_1_SiIn";
    url += "&prompt=login";
    return url;
};

Dfiow.prototype.isDFIAzureB2CIDTokenExsit = function () {
    return (this.b2c_token != "" ? true : false);
};

Dfiow.prototype.getDFIAzureB2CIDToken = function () {
    return this.b2c_token;
};

Dfiow.prototype.getDFIUserAuthorization = function (SystemName) {
    var url = this.host_url + "/DesktopModules/PartnerZone/API/data/get";
    url += "?system=" + SystemName;
    url += "&functions=true&groups=true";
    return get(url, dfiUserAuthSuccess);
};

Dfiow.prototype.getDFIPZGroupList = function (SystemName) {
    var url = this.host_url + "/DesktopModules/PartnerZone/API/data/get";
    url += "?system=" + SystemName;
    url += "&groups=true";
    return get(url, dfiGroupListSuccess);
};

function get(url, successFunction) {
    var result;
    $.support.cors = true;
    var options = {
        type: "GET",
        url: url,
        contentType: "application/json",
        dataType: "json",
        async: false,
        xhrFields: { withCredentials: true },
        success: function (data, textStatus) {
            result = successFunction(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            return "Error";
        }
    };

    $.ajax(options);
    return result;
};

function dfiTokenSuccess(result) {
    if (result.token != null) {
        return result.token;
    }
};

function dfiUserAuthSuccess(result) {
    if (result != null) {
        return result;
    }
};

function dfiGroupListSuccess(result) {
    if (result != null) {
        return result;
    }
};